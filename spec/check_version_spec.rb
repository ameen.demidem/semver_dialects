# frozen_string_literal: true

require 'semver_dialects/commands/check_version'

RSpec.describe SemverDialects::Commands::CheckVersion do
  it 'executes `check_version` command successfully' do
    output = StringIO.new
    type = 'maven'
    version = '1.2'
    constraint = '[,2.5]'
    options = {}
    command = described_class.new(type, version, constraint, options)

    command.execute(output: output)

    expect(output.string).to eq("1.2 matches [,2.5] for maven\n")
  end
end
