# frozen_string_literal: true

RSpec.describe SemverDialects::Interval do
  describe '.from_version' do
    it 'returns an interval that starts and ends with that version' do
      version = SemverDialects::SemanticVersion.new('1.2.3')
      interval = described_class.from_version(version)
      expect(interval.start_cut).to eq(SemverDialects::Boundary.new(version))
      expect(interval.end_cut).to eq(SemverDialects::Boundary.new(version))
      expect(interval.bit_set?(SemverDialects::IntervalType::LEFT_CLOSED)).to be true
      expect(interval.bit_set?(SemverDialects::IntervalType::RIGHT_CLOSED)).to be true
    end
  end

  context 'Version interval operations' do
    it 'intersect should work' do
      typ = 'npm'
      vi1 = SemverDialects::IntervalParser.parse(typ, '=3.2.8')
      vi2 = SemverDialects::IntervalParser.parse(typ, '=3.2.9')
      vi3 = described_class.new(
        SemverDialects::IntervalType::LEFT_CLOSED | SemverDialects::IntervalType::RIGHT_CLOSED,
        SemverDialects::Boundary.new(SemverDialects::Semver2::VersionParser.parse('3.2.8.0')),
        SemverDialects::Boundary.new(SemverDialects::Semver2::VersionParser.parse('3.2.9.0'))
      )
      expect(vi3.start_cut).to eq(vi1.start_cut)
      expect(vi3.end_cut).to eq(vi2.start_cut)
      expect(vi3.intersect(vi2)).to eq(vi2)
      expect(vi3.intersect(vi1)).to eq(vi1)
    end
  end
end
