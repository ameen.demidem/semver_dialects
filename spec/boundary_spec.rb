# frozen_string_literal: true

RSpec.describe SemverDialects::Boundary do
  def cut_for_version(version_string)
    described_class.new(SemverDialects::SemanticVersion.new(version_string))
  end

  context '#to_s' do
    subject { described_class.new(version).to_s }

    let(:version) { double('version', to_s: version_string) }
    let(:version_string) { '1.2.3-alpha.1' }

    it 'delegates to version' do
      is_expected.to eq(version_string)
    end
  end

  context 'Version comparison' do
    it 'should work for >' do
      expect(cut_for_version('3')).to be > cut_for_version('2')
    end

    it 'should work for <' do
      expect(cut_for_version('2.3.1')).to be < cut_for_version('2.3.2')
      expect(cut_for_version('2.3.x')).to be < cut_for_version('2.3.7')
      expect(cut_for_version('2.4.0-alpha')).to be < cut_for_version('2.4.0-beta')
      expect(cut_for_version('3.0.0a1')).to be < cut_for_version('3.0.0RC4')
      expect(cut_for_version('1.3.0dev0')).to be < cut_for_version('1.3.0b3')
      expect(cut_for_version('1.2-alpha-6')).to be < cut_for_version('1.2-beta-2')
      expect(cut_for_version('2.1')).to be < cut_for_version('2.2')
    end

    it 'should work for >=' do
      expect(cut_for_version('2')).to be >= cut_for_version('2')
      expect(cut_for_version('2')).to be >= cut_for_version('1')
    end

    it 'should work for <=' do
      expect(cut_for_version('2')).to be <= cut_for_version('2')
      expect(cut_for_version('1')).to be <= cut_for_version('2')
    end

    it 'should work for ==' do
      expect(cut_for_version('4.2')).to eq(cut_for_version('4.2.0.RELEASE'))
      expect(cut_for_version('1')).to eq(cut_for_version('1'))
    end
  end

  context 'version cuts' do
    it 'should correctly identify initial versions' do
      vc0 = cut_for_version('0.0.0')
      expect(vc0.is_initial_version?).to eq(true)

      vc1 = cut_for_version('0')
      expect(vc1.is_initial_version?).to eq(true)

      vc2 = cut_for_version('1.2.3')
      expect(vc2.is_initial_version?).to eq(false)

      vc3 = cut_for_version('0.rc1')
      expect(vc3.is_initial_version?).to eq(true)
    end
  end
end
