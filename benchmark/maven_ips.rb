# frozen_string_literal: true

require 'bundler/setup'
require 'semver_dialects'
require 'benchmark/ips'

Benchmark.ips do |x|
  [
    %w[release 1.2.3],
    %w[prerelease 1.2.3a1],
    %w[complex 1.2.3a4.5.6-beta7.8-milestone9-60-2173855]
  ].each do |(name, version)|
    x.report("maven-#{name}") { Maven::VersionParser.parse(version) }
    x.report("legacy-#{name}") { SemanticVersion.new(version) }
  end
end
