# frozen_string_literal: true

# Boundary is a boundary used in an interval.
# It can either be above all versions (infinity),
# below all versions (negative infinity), or any version.
module SemverDialects
  class Boundary
    attr_accessor :semver

    def initialize(semver)
      @semver = semver
    end

    def to_s
      @semver.to_s
    end

    def <(other)
      if other.instance_of?(BelowAll)
        false
      else
        other.instance_of?(AboveAll) ? true : @semver < other.semver
      end
    end

    def >(other)
      if other.instance_of?(BelowAll)
        true
      else
        other.instance_of?(AboveAll) ? false : @semver > other.semver
      end
    end

    def <=(other)
      self < other || self == other
    end

    def >=(other)
      self > other || self == other
    end

    def ==(other)
      # self cannot be BelowAll or AboveAll
      if other.instance_of?(BelowAll) || other.instance_of?(AboveAll)
        false
      else
        @semver == other.semver
      end
    end

    def !=(other)
      # self cannot be BelowAll or AboveAll
      if other.instance_of?(BelowAll) || other.instance_of?(AboveAll)
        false
      else
        @semver != other.semver
      end
    end

    def is_initial_version?
      @semver.is_zero?
    end
  end

  # BelowAll represents a boundary below all possible versions.
  # When used as the lower boundary of an interval, any version
  # that is smaller than the upper boundary is in the interval.
  class BelowAll < Boundary
    def initialize; end

    def to_s
      '-inf'
    end

    def is_initial_version?
      false
    end

    def <(other)
      other.instance_of?(BelowAll) ? false : true
    end

    def >(_other)
      false
    end

    def <=(other)
      self < other || self == other
    end

    def >=(other)
      self > other || self == other
    end

    def ==(other)
      (other.instance_of? BelowAll) ? true : false
    end

    def !=(other)
      !(self == other)
    end
  end

  # AboveAll represents a boundary above all possible versions.
  # When used as the upper boundary of an interval, any version
  # that is greater than the lower boundary is in the interval.
  class AboveAll < Boundary
    def initialize; end

    def to_s
      '+inf'
    end

    def is_initial_version?
      false
    end

    def <(_other)
      false
    end

    def >(other)
      other.instance_of?(AboveAll) ? false : true
    end

    def <=(other)
      self < other || self == other
    end

    def >=(other)
      self > other || self == other
    end

    def ==(other)
      (other.instance_of? AboveAll) ? true : false
    end

    def !=(other)
      !(self == other)
    end
  end
end
